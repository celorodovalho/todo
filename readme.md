- composer install
- php -r "file_exists('.env') || copy('.env.example', '.env');"
- php artisan key:generate
- sudo chmod -R ug+rwx storage bootstrap/cache
- php artisan migrate
- OPTIONAL: php artisan db:seed
- php artisan serve
- GO TO: http://127.0.0.1:8000/tasks to test over Front-end (Vue.js)
- API Routes:
    - LIST: GET /api/tasks/ - http://127.0.0.1:8000/api/tasks
    - SHOW: GET /api/tasks/{uuid} - http://127.0.0.1:8000/api/tasks/{uuid}
    - DESTROY: DELETE /api/tasks/{uuid} - http://127.0.0.1:8000/api/tasks/{uuid}
    - UPDATE: PUT /api/tasks/ -  http://127.0.0.1:8000/api/tasks
    - CREATE: POST /api/tasks/ -  http://127.0.0.1:8000/api/tasks
    - ORDER: PUT /api/tasks/order - http://127.0.0.1:8000/api/tasks/order

UPDATE/CREATE Entity:
```json
{
   "uuid": "",
   "type": "",
   "content": "",
   "done" : true|false
}
```

ORDER Entity:
```json
{
   "uuids": []
}
```
Ex:
```{"uuids":[3,1,2]}```
- the record for model uuid 3 will have sort_order value 1
- the record for model uuid 1 will have sort_order value 2
- the record for model uuid 2 will have sort_order value 3


DEMO: http://dev.marcelorodovalho.com/workspace/todo/public/tasks

Test part 2:
- https://bitbucket.org/celorodovalho/todo/src/6088f332cdae9048222176ca953d01ace6ab0664/app/CreditCard.php
- https://bitbucket.org/celorodovalho/todo/src/6088f332cdae9048222176ca953d01ace6ab0664/app/CreditCardTest.php