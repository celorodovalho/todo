<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('tasks')->group(function () {
// get list of tasks /api/tasks/
    Route::get('/', 'TaskController@index');
// get specific task /api/tasks/{uuid}
    Route::get('{uuid}', 'TaskController@show');
// delete a task /api/tasks/{uuid}
    Route::delete('{uuid}', 'TaskController@destroy');
// update existing task /api/tasks/
    Route::put('/', 'TaskController@store');
// create new task /api/tasks/
    Route::post('/', 'TaskController@store');
// order tasks /api/order/
    Route::put('order', 'TaskController@order');
});
