<!doctype html>
<html data-framework="vue">
<head>
    <meta charset="utf-8">
    <title>TODO List</title>
    <base href="{{ asset('/') }}" />
    <link rel="stylesheet" href="https://unpkg.com/todomvc-app-css@2.0.4/index.css">
    <style>[v-cloak] {
            display: none;
        }</style>
    <style>
        label small {
            font-size: 10px;
            color: #fff;
            background-color: #b83f45;
        }
    </style>
    <script>
        var API_URL = "{{ asset('/') }}";
    </script>
</head>
<body>
<section class="todoapp">
    <header class="header">
        <h1>todos</h1>
        <select class="new-todo"
                style="width:50%;float: left"
                :value="newTodo.type"
                @change="setCurrentType">
            <option value="shopping">Shopping</option>
            <option value="work">Work</option>
        </select>
        <input class="new-todo"
               style="width:50%"
               autofocus autocomplete="off"
               placeholder="Content"
               :value="newTodo.content"
               @change="setNewTodo"
               @keyup.enter="addTodo"
        >
    </header>
    <h2 v-show="loading === true" style="text-align: center">Loading...</h2>
    <h3 v-show="message.length" style="text-align: center">@{{message}}</h3>
    <section class="main" v-show="todos.length" v-cloak>
        <ul class="todo-list">
            <draggable :list="todos" class="dragArea" @end="updateOrder">
                <li v-for="todo in todos"
                    class="todo"
                    :key="todo.uuid"
                    :class="{ completed: todo.done }">
                    <div class="view">
                        <input class="toggle" type="checkbox" v-model="todo.done" @change="setDoneTodo(todo)">
                        <label>
                            <small>[@{{ todo.type }}]</small>
                            <span v-show="!todo.edit" @click="(e) => {toggleEdit(e, todo)}">@{{ todo.content }}</span>
                            <input type="text"
                                   v-model="todo.content"
                                   v-show="todo.edit"
                                   @blur="(e) => {updateTodo(e, todo)}">
                        </label>
                        <button class="destroy" @click="removeTodo(todo)"></button>
                    </div>
                </li>
            </draggable>
        </ul>
    </section>
</section>

<script src="https://unpkg.com/babel-polyfill@latest/dist/polyfill.min.js"></script>
<script src="https://unpkg.com/vue@2.5.5/dist/vue.js"></script>
<script src="https://unpkg.com/vuex@3.0.1/dist/vuex.js"></script>
<script src="https://unpkg.com/axios@0.17.1/dist/axios.min.js"></script>
<script src="{{ asset('/').mix('/js/app.js') }}"></script>
</body>
</html>

