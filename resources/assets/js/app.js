/* global Vue, Vuex, axios, FileReader */
import draggable from 'vuedraggable';

(function () {
    Vue.use(Vuex);

    const store = new Vuex.Store({
        state: {
            loading: true,
            message: '',
            todos: [],
            newTodo: {
                content: '',
                type: 'work'
            },
            order: []
        },
        getters: {
            newTodo: state => state.newTodo,
            todos: state => state.todos,
            loading: state => state.loading,
            message: state => state.message
        },
        mutations: {
            SET_LOADING(state, flag) {
                state.loading = flag
            },
            SET_MESSAGE(state, message) {
                state.message = message;
            },
            SET_TODOS(state, todos) {
                console.log(todos);
                if (todos.data.length) {
                    state.todos = todos.data
                } else if (todos.meta.message) {
                    state.message = todos.meta.message;
                }
            },
            SET_NEW_TODO(state, todo) {
                state.newTodo.content = todo
            },
            SET_CURRENT_TYPE(state, type) {
                state.newTodo.type = type
            },
            UPDATE_ORDER(state, uuids) {
                state.order = uuids
            },
            UPDATE_TODO(state, todo) {
                //state.order = uuids
            },
            ADD_TODO(state, todoObject) {
                state.todos.push(todoObject)
            },
            REMOVE_TODO(state, todo) {
                let todos = state.todos;
                todos.splice(todos.indexOf(todo), 1)
            },
            CLEAR_NEW_TODO(state) {
                state.newTodo = {
                    content: '',
                    type: 'work'
                };
                console.log('clearing new todo')
            },
            SET_DONE_TODO(state) {
                console.log('done/undone todo')
            }
        },
        actions: {
            loadTodos({commit}) {
                commit('SET_LOADING', true);
                axios
                    .get(API_URL + 'api/tasks')
                    .then(r => r.data)
                    .then(todos => {
                        commit('SET_TODOS', todos);
                        commit('SET_MESSAGE', '');
                        commit('SET_LOADING', false)
                    })
            },
            setNewTodo({commit}, todo) {
                commit('SET_NEW_TODO', todo)
            },
            setCurrentType({commit}, type) {
                commit('SET_CURRENT_TYPE', type)
            },
            updateOrder({commit}, uuids) {
                axios.put(API_URL + 'api/tasks/order', {uuids: uuids}).then(_ => {
                    commit('SET_MESSAGE', '');
                    commit('UPDATE_ORDER', uuids)
                }).catch(error => {
                    commit('SET_MESSAGE', error.response.data.error.message);
                })
            },
            setDoneTodo({commit}, todo) {
                axios.put(API_URL + 'api/tasks', todo).then(_ => {
                    commit('SET_MESSAGE', '');
                    commit('SET_DONE_TODO', todo)
                })
            },
            updateTodo({commit}, todo) {
                /*if (!todo.content) {
                    // do not add empty todos
                    return
                }*/
                axios.put(API_URL + 'api/tasks', todo).then(_ => {
                    commit('SET_MESSAGE', '');
                    commit('UPDATE_TODO', todo)
                })
            },

            addTodo({commit, state}) {
                console.log(state.newTodo);
                /*if (!state.newTodo.content) {
                    // do not add empty todos
                    return
                }*/
                const todo = {
                    type: state.newTodo.type,
                    content: state.newTodo.content,
                    done: false
                };
                axios.post(API_URL + 'api/tasks', todo).then(response => {
                    commit('SET_MESSAGE', '');
                    commit('ADD_TODO', response.data.data)
                }).catch(error => {
                    commit('SET_MESSAGE', error.response.data.error.message);
                })
            },
            removeTodo({commit}, todo) {
                axios.delete(API_URL + 'api/tasks/' + todo.uuid).then(_ => {
                    console.log('removed todo', todo.uuid, 'from the server');
                    commit('SET_MESSAGE', '');
                    commit('REMOVE_TODO', todo)
                }).catch(error => {
                    commit('SET_MESSAGE', error.response.data.error.message);
                })
            },
            clearNewTodo({commit}) {
                commit('CLEAR_NEW_TODO')
            }
        }
    });

    // app Vue instance
    const app = new Vue({
        store,
        components: {
            draggable,
        },
        data: {
            file: null
        },
        el: '.todoapp',

        created() {
            this.$store.dispatch('loadTodos')
        },

        // computed properties
        // https://vuejs.org/guide/computed.html
        computed: {
            newTodo() {
                return this.$store.getters.newTodo
            },
            todos() {
                return this.$store.getters.todos
            },
            loading() {
                return this.$store.getters.loading
            },
            message() {
                return this.$store.getters.message
            }
        },

        // methods that implement data logic.
        // note there's no DOM manipulation here at all.
        methods: {
            setNewTodo(e) {
                this.$store.dispatch('setNewTodo', e.target.value)
            },

            setCurrentType(e) {
                this.$store.dispatch('setCurrentType', e.target.value)
            },

            updateOrder(e) {
                let uuids = [];
                for (let todo in this.$store.state.todos) {
                    if (this.$store.state.todos.hasOwnProperty(todo)) {
                        uuids.push(this.$store.state.todos[todo].uuid);
                    }
                }
                this.$store.dispatch('updateOrder', uuids)
            },

            setDoneTodo(todo) {
                this.$store.dispatch('setDoneTodo', todo)
            },

            addTodo(e) {
                e.target.value = '';
                this.$store.dispatch('addTodo');
                this.$store.dispatch('clearNewTodo')
            },

            removeTodo(todo) {
                this.$store.dispatch('removeTodo', todo)
            },

            uploadTodos(e) {
                // either set component data.file to test file
                // or read it off the native event
                const f = this.file || e.target.files[0];
                const reader = new FileReader();
                reader.onload = e => {
                    const list = JSON.parse(e.target.result);
                    list.forEach(todo => {
                        this.$store.commit('ADD_TODO', todo)
                    })
                };
                reader.readAsText(f)
            },
            toggleEdit(e, todo) {
                Vue.set(todo, 'edit', !todo.edit);
                //todo.edit = !todo.edit;

                // Focus input field
                if (todo.edit) {
                    Vue.nextTick(function () {
                        e.target.nextElementSibling.focus();
                    })
                }
            },
            updateTodo(e, todo) {
                this.$store.dispatch('updateTodo', todo);
                this.toggleEdit(e, todo);
            }
        }
    });

    window.app = app
})();