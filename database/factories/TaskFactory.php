<?php
// app/database/factories/TaskFactory.php
use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'type' => $faker->unique()->name,
        'content' => $faker->text,
        'done' => false
    ];
});
