let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
/*.options({
    uglify: {
        uglifyOptions: {
            warnings: false,
            comments: false,
            beautify: false,
            compress: {
                drop_console: true,
            }
        }
    },
})*/
mix.js('resources/assets/js/app.js', 'public/js').version();
// mix.scripts([
//     'node_modules/todomvc-common/base.js',
//     'node_modules/director/build/director.js',
//     'node_modules/vue/dist/vue.js',
//     'node_modules/vue-resource/dist/vue-resource.js',
//     'resources/assets/js/app.js'
// ], 'public/js/app.js')
mix.styles([
        'node_modules/todomvc-common/base.css',
        'node_modules/todomvc-app-css/index.css'
    ], 'public/css/app.css');
