<?php namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class TaskTransformer extends TransformerAbstract
{

    public function transform($task)
    {
        return [
            'uuid' => $task->uuid,
            'type' => $task->type,
            'content' => $task->content,
            'sort_order' => $task->sort_order,
            'done' => (bool)$task->done,
            'date_created' => $task->date_created
        ];
    }
}
