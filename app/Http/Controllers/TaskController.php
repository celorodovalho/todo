<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Task;
use App\Transformer\TaskTransformer;

/**
 * Class TaskController
 * @package App\Http\Controllers
 */
class TaskController extends Controller
{
    protected $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * List all tasks in the todo list
     *
     * @return mixed
     */
    public function index()
    {
        //Get all task
        $tasks = Task::paginate(999);
        $meta = ['message' => 'Success!'];
        if (\count($tasks) <= 0) {
            $meta['message'] = 'Wow. You have nothing else to do. Enjoy the rest of your day!';
        }

        // Return a collection of $task with pagination
        return $this->response->withPaginator($tasks, new  TaskTransformer(), null, $meta);
    }

    /**
     * Describe a single task by uuid
     *
     * @param $uuid
     *
     * @return mixed
     */
    public function show($uuid)
    {
        //Get the task
        $task = Task::find($uuid);
        if (!$task) {
            return $this->response->errorNotFound('Task Not Found');
        }
        // Return a single task
        return $this->response->withItem($task, new  TaskTransformer());
    }

    /**
     * Delete a single task by uuid
     *
     * @param $uuid
     *
     * @return mixed
     */
    public function destroy($uuid)
    {
        //Get the task
        $task = Task::find($uuid);
        if (!$task) {
            return $this->response->errorNotFound(
                'Good news! The task you were trying to delete didn\'t even exist.'
            );
        }

        if ($task->delete()) {
            return $this->response->withItem($task, new  TaskTransformer());
        } else {
            return $this->response->errorInternalError('Could not delete a task');
        }
    }

    /**
     * Insert/update a single task
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        if ($request->isMethod('put')) {
            //Get the task
            $task = Task::find($request->uuid);
            if (!$task) {
                return $this->response->errorNotFound(
                    'Are you a hacker or something? The task you were trying to edit doesn\'t exist.'
                );
            }
        } else {
            $task = new Task;
        }

        $task->type = $request->input('type');
        if (!\in_array($task->type, ['shopping', 'work'], true)) {
            return $this->response->errorInternalError(
                'The task type you provided is not supported. You can only use shopping or work.'
            );
        }
        $task->content = $request->input('content');
        if (empty(trim($task->content))) {
            return $this->response->errorInternalError(
                'Bad move! Try removing the task instead of deleting its content.'
            );
        }
        $task->done = $request->input('done');

        if ($task->save()) {
            return $this->response->withItem($task, new  TaskTransformer());
        } else {
            return $this->response->errorInternalError(
                'Bad move! Try removing the task instead of deleting its content.'
            );
        }
    }

    /**
     * Order all tasks by uuids
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function order(Request $request)
    {
        $uuids = $request->uuids;
        $tasks = Task::whereIn('uuid', $uuids)->count();
        if ($tasks === count($uuids)) {
            return $this->response->errorInternalError(
                "Are you a hacker or something? The task you were trying to edit doesn't exist."
            );
        }
        Task::setNewOrder($uuids);
        $tasks = Task::where('uuid', '>', 0)->pluck('uuid')->toArray();
        return $this->response->withArray($tasks);
    }
}
