<?php

namespace App;

class CreditCard
{
    private $number;
    private $error;

    private function checkLength($length, $category)
    {
        switch ($category) {
            case 0:
                return \in_array($length, [13, 16], true);
                break;
            case 1:
                return \in_array($length, [16, 18, 19], true);
                break;
            case 2:
                return $length === 16;
                break;
            case 3:
                return $length === 15;
                break;
            case 4:
                return $length === 14;
                break;
            default:
                return 1;
        }
    }

    /**
     * Check if each character is a digit, space or punctuation
     *
     * @param string $number
     * @return bool
     */
    private function checkDigits($number)
    {
        $arrNumbers = str_split($number);
        // check input
        foreach ($arrNumbers as $char) {
            // is a digit?
            if (!ctype_digit($char) && !ctype_space($char) && !ctype_punct($char)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the Card Number is a valid number
     *
     * @param null $number
     * @return bool|string
     */
    public function isValid($number = null)
    {
        // new validation?
        if (null !== $number) {
            // clear current object values ... can't set anything until
            // the card number is identified ...
            $this->number = null;
            $this->error = 'ERROR_NOT_SET';

            /**
             * Visa = 4XXX - XXXX - XXXX - XXXX
             * MasterCard = 5[1-5]XX - XXXX - XXXX - XXXX
             * Discover = 6011 - XXXX - XXXX - XXXX
             * Amex = 3[4,7]X - XXXX - XXXX - XXXX
             * Diners = 3[0,6,8] - XXXX - XXXX - XXXX
             * Any Bankcard = 5610 - XXXX - XXXX - XXXX
             * JCB =  [3088|3096|3112|3158|3337|3528] - XXXX - XXXX - XXXX
             * Enroute = [2014|2149] - XXXX - XXXX - XXX
             * Switch = [4903|4911|4936|5641|6333|6759|6334|6767] - XXXX - XXXX - XXXX
             */
            // The most safe way the get the first char of a string
            $firstChar = mb_substr($number, 0, 1, 'utf-8');
            switch ($firstChar) {
                case '4':
                case '5':
                    $lencat = 2;
                    break;
                case '3':
                    $lencat = 4;
                    break;
                case '2':
                    $lencat = 3;
                    break;
                default:
                    $lencat = null;
            }

            if (!$this->checkDigits($number)) {
                $this->error = 'ERROR_INVALID_CHAR';
            } elseif (!$this->checkLength(\strlen($number), $lencat)) {
                $this->error = 'ERROR_INVALID_LENGTH';
            } else {
                $this->number = $number;
                $this->error = true;
            }
        } else {
            $this->error = 'ERROR_INVALID_CHAR';
        }
        return $this->error;
    }

    public function set($number)
    {
        // anything passed?
        if (null !== $number) {
            // yes, check/update the number
            return $this->isValid($number);
        }
        $this->error = 'ERROR_NOT_SET';
        return $this->error;
    }

    /**
     * Retrieve the current card number. The number is returned unformatted
     * suitable for use with submission to payment and authorization gateways.
     *
     * @return mixed Card Number
     */
    public function get()
    {
        return $this->number;
    }
}
